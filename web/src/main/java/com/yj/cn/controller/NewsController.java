package com.yj.cn.controller;

import com.yj.cn.dto.NewsDetail;
import com.yj.cn.feign.NewsFeign;
import com.yj.cn.utils.ReturnResult;
import com.yj.cn.utils.ReturnResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api("新闻管理")
@RestController
public class NewsController {

    @Autowired
    private NewsFeign newsFeign;

    @ApiOperation("按分类查询")
    @RequestMapping(value = "/queryByCategory")
    public ReturnResult<List<NewsDetail>> queryByCategory(@RequestParam(value = "num", required = false) Integer num) {
        return newsFeign.queryByCategory(num);
    }
    @ApiOperation("模糊查询")
    @RequestMapping(value = "/queryByCategoryAndWords")
    public ReturnResult<List<NewsDetail>> queryByCategoryAndWords(@RequestParam(value = "num", required = false) Integer num, @RequestParam(value = "search", required = true) String search) {
      return newsFeign.queryByCategoryAndWords(num,search);
    }

    @ApiOperation("修改新闻")
    @RequestMapping(value = "/updateNew")
    public ReturnResult updateNew(@RequestParam(value = "id",required = true) Integer id,@RequestBody NewsDetail newsDetail) {
      return newsFeign.updateNew(id,newsDetail);
    }

}
