package com.yj.cn.conf;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.yj.cn.mapper")
public class MybatisConfiguration {
}
