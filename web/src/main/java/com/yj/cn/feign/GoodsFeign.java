package com.yj.cn.feign;

import com.yj.cn.utils.ReturnResult;
import com.yj.cn.utils.ReturnResultUtils;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


@FeignClient(name = "SERVICE",fallback = GoodsFeign.GoodsHystrix.class)
public interface GoodsFeign {

    @GetMapping(value = "api/test")
    int getGoods(@RequestParam(value = "num") int num);

    @GetMapping(value = "api/login")
    String userLogin();

    @GetMapping(value = "api/hello")
    String helloWorld();

    @Component
    public class GoodsHystrix implements GoodsFeign{
        @Override
        public int getGoods(@RequestParam(value = "num")int num) {
            return 0;
        }

        @Override
        public String userLogin() {
            return "1";
        }

        @Override
        public String helloWorld() {
            return "hello";
        }


    }
}
