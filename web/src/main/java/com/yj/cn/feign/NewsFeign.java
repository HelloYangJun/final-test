package com.yj.cn.feign;

import com.yj.cn.dto.NewsDetail;
import com.yj.cn.service.NewsService;
import com.yj.cn.utils.ReturnResult;
import com.yj.cn.utils.ReturnResultUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name = "SERVICE")
public interface NewsFeign {

    @GetMapping("news/queryByCategory")
    ReturnResult<List<NewsDetail>> queryByCategory(@RequestParam(value = "num", required = false) Integer num);

    @GetMapping("news/queryByCategoryAndWords")
    ReturnResult<List<NewsDetail>> queryByCategoryAndWords(@RequestParam(value = "num", required = false) Integer num, @RequestParam(value = "search", required = true) String search);

    @GetMapping("news/updateNew")
    ReturnResult updateNew(@RequestParam(value = "id",required = true) Integer id, @RequestBody NewsDetail newsDetail);

    @Component
    class NewsHystrix implements NewsFeign{
        @Autowired
        private NewsService newsService;

        @ApiOperation("按分类查询")
        @RequestMapping(value = "/queryByCategory")
        public ReturnResult<List<NewsDetail>> queryByCategory(@RequestParam(value = "num", required = false,name = "类别") Integer num) {
            List<NewsDetail> newsDetailList = newsService.queryByCategory(num);
            return ReturnResultUtils.returnSuccess(newsDetailList);
        }

        @ApiOperation("模糊查询")
        @RequestMapping(value = "/queryByCategoryAndWords")
        public ReturnResult<List<NewsDetail>> queryByCategoryAndWords(@RequestParam(value = "num", required = false) Integer num, @RequestParam(value = "search", required = true) String search) {
            List<NewsDetail> newsDetailList = newsService.queryByCategoryAndWords(num, search);
            if (!CollectionUtils.isEmpty(newsDetailList)) return ReturnResultUtils.returnSuccess(newsDetailList);
            return ReturnResultUtils.returnFail(444,"对不起，没有相关数据");

        }

        @ApiOperation("修改新闻")
        @RequestMapping(value = "/updateNew")
        public ReturnResult updateNew(@RequestParam(value = "id",required = true) Integer id,@RequestBody NewsDetail newsDetail) {
            if (!CollectionUtils.isEmpty(newsService.queryByTitle(newsDetail))) return ReturnResultUtils.returnFail(445,"已存在该标题");
            Boolean isSuccess = newsService.updateNews(id,newsDetail) == 1 ? true : false;
            if (isSuccess) return ReturnResultUtils.returnSuccess("修改成功");
            return ReturnResultUtils.returnFail(400, "修改失败");

        }

    }

}
