package com.yj.cn.service;

import com.yj.cn.dto.NewsCategoryExample;
import com.yj.cn.dto.NewsDetail;
import com.yj.cn.dto.NewsDetailExample;
import com.yj.cn.mapper.NewsDetailMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class NewsService {

    @Autowired
    private NewsDetailMapper newsDetailMapper;

    /**
     * 分类查询
     *
     * @param num
     * @return
     */
    public List<NewsDetail> queryByCategory(Integer num) {
        NewsDetailExample newsDetailExample = new NewsDetailExample();
        newsDetailExample.setOrderByClause("'id' ASC");
        if (num == 0 || StringUtils.isEmpty(num)) return newsDetailMapper.selectByExample(newsDetailExample);
        newsDetailExample.createCriteria().andCategoryidEqualTo(num);
        List<NewsDetail> newsDetailList = newsDetailMapper.selectByExample(newsDetailExample);
        return newsDetailList;
    }

    /**
     * 模糊查询
     *
     * @param num
     * @param words
     * @return
     */
    public List<NewsDetail> queryByCategoryAndWords(Integer num, String words) {
        NewsDetailExample newsDetailExample = new NewsDetailExample();
        NewsDetailExample.Criteria criteria = newsDetailExample.createCriteria();
        if (!StringUtils.isEmpty(num)) {
            criteria.andCategoryidEqualTo(num);
        }
        if (!StringUtils.isEmpty(words)) {
            criteria.andTitleLike("%" + words + "%");
        }
        List<NewsDetail> newsDetailList = newsDetailMapper.selectByExample(newsDetailExample);
        return newsDetailList;
    }

    /**
     * 修改
     * @param newsDetail
     * @return
     */
    public int updateNews(Integer id,NewsDetail newsDetail) {
        NewsDetailExample newsDetailExample = new NewsDetailExample();
        newsDetailExample.createCriteria().andIdEqualTo(id);
        return newsDetailMapper.updateByExampleSelective(newsDetail, newsDetailExample);
    }

    /**
     * 查询新闻标题是否一样
     */
    public List<NewsDetail> queryByTitle(NewsDetail newsDetail){
        NewsDetailExample newsDetailExample = new NewsDetailExample();
        newsDetailExample.createCriteria().andTitleEqualTo(newsDetail.getTitle());
        List<NewsDetail> newsDetailList = newsDetailMapper.selectByExample(newsDetailExample);
        return newsDetailList;
    }

}
