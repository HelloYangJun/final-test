package com.yj.cn.mapper;

import com.yj.cn.dto.NewsDetail;
import com.yj.cn.dto.NewsDetailExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface NewsDetailMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table news_detail
     *
     * @mbg.generated
     */
    long countByExample(NewsDetailExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table news_detail
     *
     * @mbg.generated
     */
    int deleteByExample(NewsDetailExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table news_detail
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table news_detail
     *
     * @mbg.generated
     */
    int insert(NewsDetail record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table news_detail
     *
     * @mbg.generated
     */
    int insertSelective(NewsDetail record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table news_detail
     *
     * @mbg.generated
     */
    List<NewsDetail> selectByExample(NewsDetailExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table news_detail
     *
     * @mbg.generated
     */
    NewsDetail selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table news_detail
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") NewsDetail record, @Param("example") NewsDetailExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table news_detail
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") NewsDetail record, @Param("example") NewsDetailExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table news_detail
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(NewsDetail record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table news_detail
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(NewsDetail record);
}