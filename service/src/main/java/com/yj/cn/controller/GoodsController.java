package com.yj.cn.controller;

import com.yj.cn.utils.ReturnResult;
import com.yj.cn.utils.ReturnResultUtils;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

@Api("人事管理")
@RestController
@RequestMapping(value = "api")
public class GoodsController {


    @GetMapping(value = "/test")
    public int testGoods(@RequestParam(value = "num") int num) {
        return 3;
    }

    @PostMapping(value = "/toLogin")
    public ReturnResult toLogin() {

        return ReturnResultUtils.returnSuccess("登录成功");
    }


}