package com.yj.cn.enums;

public interface IErrorCode {
    String getErrorCode();
    String getMessage();
}
